
==============================
Views UI: Edit Headers/Footers
==============================
Compatibility: Views 6.x-2.x

   http://drupal.org/project/views_headers_footers

This module provides a separate interface that displays a list of views (defined by you, so you
can exclude certain views) and allows users with the correct permission to modify the header and 
footer content.

======
How-To
======
- The edit page is accessed via "Content management" --> "Edit Views Headers/Footers."
- To define which views to display, visit "Site Confirugation" --> "Views" --> "Editable headers/footers"
- Make sure the user role has the "edit views headers/footers" permission.

Please note that the "override" button will be hidden, so if your view uses multiple displays, 
you will probably want to modify the header/footer fields to be "overriden," otherwise it will 
update the default display when the user goes in and modifies them.